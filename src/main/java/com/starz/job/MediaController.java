package com.starz.job;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.stream.Collectors;

@RestController
public class MediaController {


    private final String url = "https://de8a7d97-b45e-401b-b30e-39ae7b922405.mock.pstmn.io/api/v1.0/mediaCatalog/titles/movies";

    @RequestMapping(value = "/media", method = RequestMethod.GET)
    public ResponseEntity<?> media(@Valid Filter filter, @Valid Level level, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            String collect = bindingResult.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(";"));
            JSONObject error = new JSONObject();
            error.put("error", collect);
            return ResponseEntity.badRequest().body(error);
        }

        if (!filter.isValid()) {
            return ResponseEntity.badRequest().body(new JSONObject().put("error", "Incorrect arguments on filter['censoring']"));
        }
        if (!level.isValid()) {
            return ResponseEntity.badRequest().body(new JSONObject().put("error", "Incorrect arguments on level['uncensored','censored']"));
        }


        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        //read JSON like DOM Parser
        JsonNode rootNode = null;
        try {
            rootNode = objectMapper.readTree(responseEntity.getBody());
            //filter
            level.entryFilter(rootNode);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(204).body(new JSONObject().put("error", "Error processing the REST endpoint"));
        }

        return ResponseEntity.ok(rootNode);
    }


}
