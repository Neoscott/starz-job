package com.starz.job;

import org.hibernate.validator.constraints.NotEmpty;

public class Filter {

    private final String filter_censoring = "censoring";

    @NotEmpty
    private String filter;

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public boolean isValid(){
        return this.filter.equals(filter_censoring);
    }

    @Override
    public String toString() {
        return "Filter{" +
                "filter='" + filter + '\'' +
                '}';
    }
}
