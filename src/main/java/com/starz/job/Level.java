package com.starz.job;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Iterator;

public class Level {

    private final String level_censored = "censored";
    private final String level_uncesored = "uncensored";

    @NotEmpty
    private String level;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


    public boolean isValid(){
        return this.level.equals(level_censored) || this.level.equals(level_uncesored);
    }


    public void entryFilter(JsonNode node){
        if(node.path("entries") == null || node.path("entries").size() == 0) return;
        JsonNode entries = node.path("entries");
        Iterator<JsonNode> elements = entries.elements();
        while(elements.hasNext()){
            JsonNode entry = elements.next();
            String contentClassification = entry.get("peg$contentClassification").asText();
            if(!contentClassification.isEmpty()) {
                if (this.level.equalsIgnoreCase(level_censored)) {
                    if (contentClassification.equalsIgnoreCase(level_censored)) {
                        mediaFilter(entry);
                    } else {
                        elements.remove();
                    }
                } else if (this.level.equalsIgnoreCase(level_uncesored)) {
                    if (contentClassification.equalsIgnoreCase(level_censored)){
                        elements.remove();
                    } else {
                        mediaFilter(entry);
                    }
                }
            }
        }
        ((ObjectNode)node).put("entryCount", node.path("entries").size());
    }


    private void mediaFilter(JsonNode entry){
        if(entry.get("media") == null) return;

        Iterator<JsonNode> medias = entry.get("media").elements();
            while (medias.hasNext()) {
                JsonNode m = medias.next();
                String guid = m.get("guid").asText();
                if (this.level.equals(level_uncesored) && guid.endsWith("C")) {
                    medias.remove();
                } else if (this.level.equals(level_censored) && !guid.endsWith("C")){
                    medias.remove();
                }
            }
    }



    @Override
    public String toString() {
        return "Level{" +
                "level='" + level + '\'' +
                '}';
    }
}
