package com.starz.job;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.net.URL;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class JobApplicationTests {

	@Test
	public void contextLoads() {
	}

	@LocalServerPort
	private int port;

	private URL base;

	private String json;

	private JsonNode rootNode;


	@Autowired
	private TestRestTemplate template;

	@Before
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + port + "/");
		this.json = "{\"entries\":[{\"peg$contentClassification\":\"\", \"media\":[{\"guid\":\"MMM\"}]}, {\"peg$contentClassification\": \"Censored\", \"media\":[{\"guid\":\"C\"}]}, {\"peg$contentClassification\":\"Uncensored\", \"media\":[{\"guid\":\"UUUC\"},{\"guid\":\"UUU\"}]}]}";


	}

	@Test
	public void checkParameters() {
		ResponseEntity<String> response = template.getForEntity(base.toString()+"media",
				String.class);
		assertTrue(response.getStatusCode().value() == 400);

		response = template.getForEntity(base.toString()+"media?filter=censoring&level=censored",
				String.class);
		assertTrue(response.getStatusCode().value() == 200);

		response = template.getForEntity(base.toString()+"media?filter=UNcensoring&level=censored",
				String.class);
		assertTrue(response.getStatusCode().value() == 400);


	}


	@Test
	public void checkFilterValues(){
		Filter filter = new Filter();

		filter.setFilter("censoring");
		assertTrue(filter.isValid());

		filter.setFilter("Censoring");
		assertFalse(filter.isValid());

		filter.setFilter("ccjjik");
		assertFalse(filter.isValid());
	}

	@Test
	public void checkLevelValues(){
		Level level = new Level();

		level.setLevel("censored");
		assertTrue(level.isValid());

		level.setLevel("uncensored");
		assertTrue(level.isValid());

		level.setLevel("NNNNN");
		assertFalse(level.isValid());
	}


	@Test
	public void checkFilterLevel() throws IOException {
		Level level = new Level();
		level.setLevel("censored");

		ObjectMapper objectMapper = new ObjectMapper();
		this.rootNode = objectMapper.readTree(this.json);

		//return "" and Censored content
		level.entryFilter(this.rootNode);
		assertTrue(this.rootNode.path("entries").size() == 2);

		//return "" and Uncensored
		this.rootNode = objectMapper.readTree(this.json);
		level.setLevel("uncensored");
		level.entryFilter(this.rootNode);
		assertTrue(this.rootNode.path("entries").size() == 2);
		assertTrue(this.rootNode.path("entries").get(0).get("peg$contentClassification").asText().equals(""));
		assertTrue(this.rootNode.path("entries").get(1).get("peg$contentClassification").asText().equals("Uncensored"));
		//remove the media with guid ending on "xxxxC"
		assertTrue(this.rootNode.path("entries").get(1).path("media").size() == 1);




	}

}
