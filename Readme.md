#Starz backend dev homework

##Framework and libraries used
The framework selected is Spring boot and JDK8.
I used minidev.json-smart to create quick json object.
```
		<dependency>
			<groupId>net.minidev</groupId>
			<artifactId>json-smart</artifactId>
			<version>RELEASE</version>
		</dependency>
```

##How to run the code
Execute
```
./mvnw spring-boot:run
```
Generate the .jar file to deploy
```
./mvnw clean package
#then you can run like
java -jar target/job-0.0.1-SNAPSHOT.jar
```

##Assumptions and other things

####REST Endpoint
The json on the request was very long to map all the field to classes, normally I map all the classes
or use pages like: http://www.jsonschema2pojo.org/ to generate all the code, in this case wasn't necessary

####Spring Boot
For the exceptions I think there are better methods than the:
```
return ResponseEntity.badRequest().body(new JSONObject().put("error", "Incorrect arguments on level['uncensored','censored']"
```



